const axios = require('axios');

String.prototype.replaceAll = function (search, replacement) {
  var target = this;
  return target.split(search).join(replacement);
};

class GLClient {
  constructor({
    baseUri,
    token
  }) {
    this.baseUri = baseUri
    this.token = token
    this.headers = {
      "Content-Type": "application/json",
      "Private-Token": token
    }
  }
  callAPI({
    method,
    path,
    data
  }) {
    return axios({
      method: method,
      url: this.baseUri + path,
      headers: this.headers,
      data: data !== null ? JSON.stringify(data) : null
    })
  }
  get({
    path
  }) {
    console.log("🌍", path)
    return this.callAPI({
      method: 'GET',
      path,
      data: null
    });
  }

  post({
    path,
    data
  }) {
    return this.callAPI({
      method: 'POST',
      path,
      data
    });
  }

}

module.exports = {
  GLClient: GLClient,
  tools: {
    urlEncodedPath: ({
      name
    }) => {
      let encoded = `${name.replaceAll("/", "%2F").replaceAll(".", "%2E").replaceAll('-', '%2D').replaceAll('_', '%5F')}`
      console.log("👋", encoded)
      return encoded
    },
    formatText: (text) => {
      return text.split("\n").map(item => item.trim()).join("\n")
    }

  }
};