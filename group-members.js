const GLClient = require("./gl-cli").GLClient;
const tools = require("./gl-cli").tools;
const config = require("./config").config;

let glClient = new GLClient({
  baseUri: `${config.url}/api/v4`,
  token: config.token
});


glClient
  .get({
    path: `/groups/${tools.urlEncodedPath({name:"fnproject-experiments"})}/members/all`
  })
  .then(response => {
    console.log(response.data)
    return response.data;
  })
  .catch(error => {
    console.error(error.response)
    console.error(error.config)
    return error;
  });
