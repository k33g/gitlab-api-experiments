module.exports = {
  config: {
    token: process.env.TOKEN || "token",
    url: process.env.GITLAB_URL || "http://gitlab.test"
  }
};
